FROM debian:latest AS nginx-vti-prep
SHELL ["/bin/bash", "-c"]
RUN apt-get clean \
&& apt-get update \
&& useradd --no-create-home nginx \
&& apt-get install -y vim wget curl git build-essential \
&& apt-get install -y libpcre3 libpcre3-dev zlib1g zlib1g-dev libluajit-5.1-dev libpam0g-dev libssl-dev libluajit-5.1-dev libpam0g-dev libexpat1-dev \
&& curl -fSL https://github.com/vozlt/nginx-module-vts/archive/v0.1.18.tar.gz | tar xzf - -C /tmp \
&& wget https://nginx.org/download/nginx-1.15.0.tar.gz \
&& tar -xf nginx-1.15.0.tar.gz -C /opt/ \
&& chmod +x /opt/nginx-1.15.0/configure \
&& cd /opt/nginx-1.15.0 \
&& ./configure --prefix=/etc/nginx --sbin-path=/usr/sbin/nginx --modules-path=/usr/lib/nginx/modules --conf-path=/etc/nginx/nginx.conf --error-log-path=/var/log/nginx/error.log --http-log-path=/var/log/nginx/access.log --pid-path=/var/run/nginx.pid --lock-path=/var/run/nginx.lock --http-client-body-temp-path=/var/cache/nginx/client_temp --http-proxy-temp-path=/var/cache/nginx/proxy_temp --http-fastcgi-temp-path=/var/cache/nginx/fastcgi_temp --http-uwsgi-temp-path=/var/cache/nginx/uwsgi_temp --http-scgi-temp-path=/var/cache/nginx/scgi_temp --user=nginx --group=nginx --with-http_ssl_module --with-http_realip_module --with-http_addition_module --with-http_sub_module --with-http_dav_module --with-http_flv_module --with-http_gunzip_module --with-http_gzip_static_module --with-http_random_index_module --with-http_secure_link_module --with-http_stub_status_module --with-http_auth_request_module --with-threads --with-stream --with-stream_ssl_module --with-http_slice_module --with-mail --with-mail_ssl_module --with-file-aio --with-http_v2_module --with-cc-opt='-g -O2 -fstack-protector-strong -Wformat -Werror=format-security -Wp,-D_FORTIFY_SOURCE=2' --with-ld-opt='-Wl,-Bsymbolic-functions -Wl,-z,relro -Wl,--as-needed' --add-module=/tmp/nginx-module-vts-0.1.18 \
&& make && make install
COPY ./nginx/nginx.conf /etc/nginx/nginx.conf

FROM nginx-vti-prep AS nginx-vti
COPY --from=nginx-vti-prep /opt/nginx-1.15.0 /opt/nginx-1.15.0 
COPY --from=nginx-vti-prep /etc/nginx/nginx.conf /etc/nginx/nginx.conf
CMD ["/usr/sbin/nginx", "-g", "daemon off;"]
